const model = {
  yearsSlider: document.getElementById("yearsSlider"),
  yearsInput: document.getElementById("yearsOfMortgage"),
  interestSlider: document.getElementById("interestSlider"),
  interestInput: document.getElementById("interestRate"),
  generalForm: document.getElementById("generalForm"),
  resultContent: document.getElementById('resultContent'),
  submitButton: document.getElementById('submitButton'),
};

const model_inputs = {
  yearsOfMortgageElement: document.getElementById("yearsOfMortgage"),
  interestRateElement: document.getElementById("interestRate"),
  loanAmountElement: document.getElementById("loanAmount"),
  annualTaxElement: document.getElementById("annualTax"),
  annualInsuranceElement: document.getElementById("annualInsurance"),
}

// Range bar handler
const rangeChangeListener = (event) => {
  const {
    target
  } = event;
  const max = Number(target.getAttribute("max"));
  const min = Number(target.getAttribute("min"));
  const value = Number(target.value);
  const percent = (value - min) / (max - min);
  const idsMapper = {
    yearsSlider: model.yearsInput,
    interestSlider: model.interestInput
  }

  idsMapper[target.id].value = value;

  target.style.backgroundImage = `-webkit-gradient(linear,
      left top,
      right top,
      color-stop(${percent}, #0077c0),
      color-stop(${percent}, #cacaca)
    )`;
}

init();

// initial function
function init() {
  const ranges = document.querySelectorAll('[type="range"]');
  // Event Listener when submits the form
  model.generalForm.addEventListener("submit", (event) => {

    model.yearsInput.innerHTML = model.yearsSlider.value;
    model.interestInput.innerHTML = model.interestSlider.value;
    event.preventDefault();
    const requiredInputs = event.target.querySelectorAll('input:required');
    let isOk = true;

    for (let i = 0; i < requiredInputs.length; i++) {

      const input = requiredInputs[i];
      const pattern = input.getAttribute("pattern");
      const regex = new RegExp(pattern);
      const exec = regex.exec(input.value);
      input.classList.remove("error");
      input.parentElement.setAttribute('caption', '');

      if (!input.value) {
        isOk = false;
        input.classList.add("error");

        if (document.body.clientWidth > 818) {
          input.parentElement.setAttribute('caption', `${input.dataset.caption} is mandatory`);
        } else {
          input.parentElement.setAttribute('caption', `Mandatory field`);
        }

      } else if (!exec) {
        isOk = false;
        input.classList.add("error");
        input.parentElement.setAttribute('caption', `${input.dataset.caption} need to be a number`);
      }
    }

    // if validations are OK
    if (isOk) {

      if (model.resultContent.classList.contains("collapsed")) {
        model.resultContent.classList.toggle('collapsed');
      }

      calculateResult();
      model.submitButton.value = "RECALCULATE";
      window.scrollBy({
        top: 250,
        left: 0,
        behaviour: 'smooth'
      });
    };
  });

  for (let i = 0; i < ranges.length; i++) {
    ranges[i].addEventListener("input", rangeChangeListener);
  }
}

// All Calculations
function calculateResult() {
  const yearsOfMortgage = model_inputs.yearsOfMortgageElement.value;
  const interestRate = model_inputs.interestRateElement.value;
  const loanAmount = model_inputs.loanAmountElement.value;
  const annualTax = model_inputs.annualTaxElement.value;
  const annualInsurance = model_inputs.annualInsuranceElement.value;
  const principleAndIterest = ((interestRate / 100) / 12) * loanAmount / (1 - Math.pow((1 + ((interestRate / 100) / 12)), -yearsOfMortgage * 12));
  const tax = annualTax / 12;
  const insurance = annualInsurance / 12;
  const totalMonthPayment = principleAndIterest + tax + insurance;

  document.getElementById("principleAndIterest").innerHTML = toCurrency(principleAndIterest);
  document.getElementById("tax").innerHTML = toCurrency(tax);
  document.getElementById("insurance").innerHTML = toCurrency(insurance);
  document.getElementById("totalMonthPayment").innerHTML = toCurrency(totalMonthPayment);
}

// Format Currency
function toCurrency(amount) {
  return "$ " + amount.toFixed(2);
};